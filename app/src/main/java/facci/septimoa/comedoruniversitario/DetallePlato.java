package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class DetallePlato extends AppCompatActivity {
    String key, imagen, nombre, precio, stock;
    TextView txtnombre, txtprecio, txtstock;
    Button openDialog;
    ImageView grid_image;
    DatabaseReference mDatabase;
    ProgressDialog progress;
    FirebaseAuth mAuth;
    FirebaseUser user;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_plato);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id2))
                .requestEmail()
                .build();
        mAuth = FirebaseAuth.getInstance();


        mDatabase = FirebaseDatabase.getInstance().getReference();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        user = mAuth.getCurrentUser();

        txtnombre = findViewById(R.id.txtnombreplato);
          txtprecio = findViewById(R.id.txtprecioplato);
          txtstock = findViewById(R.id.txtstockplatos);
          grid_image = findViewById(R.id.grid_image);
        openDialog = findViewById(R.id.open_dialog);
        progress = new ProgressDialog(this);

        openDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCustomDialog();
            }
        });



        Bundle bundle = getIntent().getExtras();
        key = bundle.getString("key");
        imagen = bundle.getString("imagen");
        nombre = bundle.getString("nombre");
        precio = bundle.getString("precio");
        stock = bundle.getString("stock");

        mostrarDetalle();


    }




    public void mostrarDetalle(){
        txtnombre.setText(nombre);
        txtprecio.setText("Precio: $"+precio);
        txtstock.setText("Stock: "+stock);
        Glide.with(DetallePlato.this).load(imagen).fitCenter().centerCrop().into(grid_image);

    }

    void showCustomDialog() {
        final Dialog dialog = new Dialog(DetallePlato.this);
        //We have added a title in the custom layout. So let's disable the default title.
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //The user will be able to cancel the dialog bu clicking anywhere outside the dialog.
        dialog.setCancelable(true);
        //Mention the name of the layout of your custom dialog.
        dialog.setContentView(R.layout.menupedir);

        //Initializing the views of the dialog.
        final EditText nameEt = dialog.findViewById(R.id.cantidadreservar);
        Button submitButton = dialog.findViewById(R.id.btnreservarpedido);


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameEt.getText().toString();

                hacerReservacion(name);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void hacerReservacion(String cantidad){

        if (Integer.parseInt(cantidad) > Integer.parseInt(stock)) {
            Toast.makeText(DetallePlato.this, "Stock no disponible", Toast.LENGTH_LONG).show();

        } else {

            progress.setMessage("Registrando en linea");
            progress.show();
            SimpleDateFormat fechaEspanol = new SimpleDateFormat("dd MMM yyyy - HH:mm:ss", Locale.forLanguageTag("es_ES"));
            Map<String, Object> map = new HashMap<>();
            map.put("nombreplato", nombre);
            map.put("cantidad", cantidad);
            map.put("precioplato", precio);
            map.put("idusuario", user.getUid());
            map.put("emailusuario", user.getEmail());
            map.put("fecha", fechaEspanol.format(new Date()));


            mDatabase.child("Reservaciones").child(fechaEspanol.format(new Date())).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                    if (task.isSuccessful()) {
                        actualizarStockPlato(key, cantidad);
                        Intent i = new Intent(DetallePlato.this, Combos.class);
                        startActivity(i);
                        finish();

                        Toast.makeText(DetallePlato.this, "Reservacion creada", Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(DetallePlato.this, "No se pudo hacer la reservacion", Toast.LENGTH_LONG).show();
                    }
                }
            });

            progress.dismiss();

            Log.d("reservacion", key);
        }
    }

    void actualizarStockPlato(String key, String cantidad){
        Map<String, Object> map = new HashMap<>();
        Integer stockActual = Integer.parseInt(stock) - Integer.parseInt(cantidad);
        map.put("nombreplato", nombre);
        map.put("precioplato", precio);
        map.put("imagenparafirebase", imagen);
        map.put("stockplatos", stockActual.toString() );
        mDatabase.child("Platos").child(key).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


            }
        });
    }


}