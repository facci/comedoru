package facci.septimoa.comedoruniversitario;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class home_admin extends AppCompatActivity {

        Button pedido, menu, codigo_qr, cerrarSesion, btnregistroplato;
        FirebaseAuth mAuth;
        String user;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home_admin);
        mAuth = FirebaseAuth.getInstance();

         user = mAuth.getCurrentUser().getEmail();

            menu = findViewById(R.id.menu);
            pedido =  findViewById(R.id.pedido);
            cerrarSesion = findViewById(R.id.cerrar_sesion);
            btnregistroplato = findViewById(R.id.btnregistroplato);

        /*    Log.d("Usuariooo",user);
            if (user == "admin@admin.com") {

           }*/
            btnregistroplato.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(home_admin.this, RegistrarPlatos.class );
                    startActivity(intent);
                }
            });


            pedido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(home_admin.this, Pedidos.class );
                    startActivity(intent);
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(home_admin.this, Combos.class );
                    startActivity(intent);
                }
            });


            // ESTE BOTON DE CERRAR LA SESION DEL USUARIO DE FIREBASE
            cerrarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FirebaseAuth.getInstance().signOut();
                    Intent i = new Intent(home_admin.this, MainActivity.class);
                    startActivity(i);
                    finish();

                }
            });
    }

}
