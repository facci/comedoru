package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class InicioSeccion extends AppCompatActivity {

    Button ingresariniciosecccion, registrariniciosecccion;
    EditText correo, password;

    FirebaseAuth mAuth;

    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio_seccion);
        mAuth = FirebaseAuth.getInstance();

        ingresariniciosecccion = findViewById(R.id.ingresariniciosecccion);
        registrariniciosecccion = findViewById(R.id.registrariniciosecccion);


        correo = findViewById(R.id.txtcorreologin);
        password = findViewById(R.id.txtcontrasenalogin);
        progress = new ProgressDialog(this);

        /*ESTA ES LA ACCION QUE HACE EL BOTON DE LOGUEARSE Y EJECUTA EL METODO DE LOGIN USUARIO*/

        ingresariniciosecccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!correo.getText().toString().isEmpty() && !password.getText().toString().isEmpty()) {


                    progress.setMessage("Iniciando");
                    progress.show();
                    mAuth.signInWithEmailAndPassword(correo.getText().toString(), password.getText().toString()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                if (task.getResult().getUser().getEmail().equals("admin@admin.com")) {
                                    Intent i = new Intent(InicioSeccion.this, home_admin.class);
                                    startActivity(i);
                                    finish();
                                }else {
                                    Intent i = new Intent(InicioSeccion.this, home.class);
                                    startActivity(i);
                                    finish();
                                }

                                Toast.makeText(InicioSeccion.this, "Bienvenido", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(InicioSeccion.this, "Credenciales incorrectas", Toast.LENGTH_LONG).show();

                            }
                            progress.dismiss();

                        }

                    });
                }else{
                    Toast.makeText(InicioSeccion.this, "Credenciales incorrectas", Toast.LENGTH_LONG).show();

                }

            }
        });

        registrariniciosecccion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(InicioSeccion.this, Registro.class );
                startActivity(intent);
            }
        });
    }
}