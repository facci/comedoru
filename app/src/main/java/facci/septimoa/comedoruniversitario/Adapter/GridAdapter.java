package facci.septimoa.comedoruniversitario.Adapter;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import facci.septimoa.comedoruniversitario.DetallePlato;
import facci.septimoa.comedoruniversitario.DetallePlato_admin;
import facci.septimoa.comedoruniversitario.R;
import facci.septimoa.comedoruniversitario.RegistrarPlatos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    Context context;
    String[] nombre;
    String[] image;
    String[] precio;
    String[] stock;
    String[] key;
    String email;

    public GridAdapter(Context context, String[] nombre, String[] image, String[] precio, String[] stock, String[] key, String email) {
        super();

        this.context = context;
        this.nombre = nombre;
        this.image = image;
        this.precio = precio;
        this.stock = stock;
        this.key = key;
        this.email = email;

    }


    @Override
    public int getCount() {
        return image.length;
    }

    @Override
    public Object getItem(int i) {
        return image[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v;

        // below line is use to inflate the
        // layout for our item of list view.
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        v = layoutInflater.inflate(R.layout.item_plato, null);
        String nombreActual = nombre[position];
        String imgActual = image[position];
        String precioActual = precio[position];
        String stockActual = stock[position];
        String keyActual = key[position];

        // after inflating an item of listview item
        // we are getting data from array list inside
        // our modal class.
        TextView txtNombre = v.findViewById(R.id.item_name);
        ImageView imgFoto = v.findViewById(R.id.grid_image);
        TextView txtprecio = v.findViewById(R.id.item_precio);

        // initializing our UI components of list view item.
        txtNombre.setText(nombreActual);
        txtprecio.setText("Precio: $"+precioActual);

        //  Picasso.get().load(imgActual).placeholder(R.drawable.img).resize(500, 500).centerCrop().into(imgFoto);
        Glide.with(context).load(imgActual).fitCenter().centerCrop().into(imgFoto);


        // below line is use to add item
        // click listener for our item of list view.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on the item click on our list view.
                // we are displaying a toast message.
               // Toast.makeText(RegistrarPlatos.this, "Imagen cargada", Toast.LENGTH_LONG).show();
                Log.d("click", "onClick: f"+ keyActual);

                if (email.equals("admin@admin.com")){

                    Intent i = new Intent(context, DetallePlato_admin.class);
                    i.putExtra("key", keyActual);
                    i.putExtra("imagen", imgActual);
                    i.putExtra("nombre", nombreActual);
                    i.putExtra("precio", precioActual);
                    i.putExtra("stock", stockActual);
                    context.startActivity(i);
                }else {
                    Intent i = new Intent(context, DetallePlato.class);
                    i.putExtra("key", keyActual);
                    i.putExtra("imagen", imgActual);
                    i.putExtra("nombre", nombreActual);
                    i.putExtra("precio", precioActual);
                    i.putExtra("stock", stockActual);
                    context.startActivity(i);
                }

            }
        });
        return v;
    }
}