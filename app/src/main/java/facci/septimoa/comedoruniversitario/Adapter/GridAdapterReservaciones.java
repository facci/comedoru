package facci.septimoa.comedoruniversitario.Adapter;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;

import facci.septimoa.comedoruniversitario.DetallePlato;
import facci.septimoa.comedoruniversitario.DetallePlato_admin;
import facci.septimoa.comedoruniversitario.R;

public class GridAdapterReservaciones extends BaseAdapter {

    Context context;
    String[] nombre;
    String[] precio;
    String[] cantidad;
    String[] email;
    String[] fecha;

    public GridAdapterReservaciones(Context context, String[] nombre, String[] precio, String[] cantidad, String[] email, String[] fecha) {
        super();

        this.context = context;
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
        this.email = email;
        this.fecha = fecha;


    }


    @Override
    public int getCount() {
        return nombre.length;
    }

    @Override
    public Object getItem(int i) {
        return nombre[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v;

        // below line is use to inflate the
        // layout for our item of list view.
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        v = layoutInflater.inflate(R.layout.item_reservacion, null);
        String nombreActual = nombre[position];
        String precioActual = precio[position];
        String cantidadActual = cantidad[position];
        String emailActual = email[position];
        String fechaActual = fecha[position];

        // after inflating an item of listview item
        // we are getting data from array list inside
        // our modal class.
        TextView txtNombre = v.findViewById(R.id.item_name_pedido);
        TextView txtcantidad = v.findViewById(R.id.item_cantidad_pedido);
        TextView txtprecio = v.findViewById(R.id.item_precio_pedido);
        TextView txttotal = v.findViewById(R.id.item_total_reservacion);
        TextView txtemail = v.findViewById(R.id.item_email_reservacion);
        TextView txtfecha = v.findViewById(R.id.item_fecha_pedido);

        // initializing our UI components of list view item.
        txtNombre.setText(nombreActual);
        txtprecio.setText("Precio: $"+precioActual);
        txtcantidad.setText("Cantidad: "+cantidadActual);
        txttotal.setText("Total: $"+Integer.parseInt(cantidadActual)*Double.parseDouble(precioActual));
        txtemail.setText("Correo: "+emailActual);
        txtfecha.setText(fechaActual);



        // below line is use to add item
        // click listener for our item of list view.
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // on the item click on our list view.
                // we are displaying a toast message.
               // Toast.makeText(RegistrarPlatos.this, "Imagen cargada", Toast.LENGTH_LONG).show();


            }
        });
        return v;
    }
}