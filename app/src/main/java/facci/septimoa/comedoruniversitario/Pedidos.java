package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import facci.septimoa.comedoruniversitario.Adapter.GridAdapter;
import facci.septimoa.comedoruniversitario.Adapter.GridAdapterReservaciones;

public class Pedidos extends AppCompatActivity {
    DatabaseReference mDatabase;
    GridView _grid;
    FirebaseAuth mAuth;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        email = mAuth.getCurrentUser().getEmail();
        _grid = findViewById(R.id.gridViewPedidos);

        obtenerPedidos();
    }

    public void obtenerPedidos(){
        mDatabase.child("Reservaciones").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists()){
                    String[] nombres = new String[ (int) snapshot.getChildrenCount()];
                    String[] cantidades = new String[ (int) snapshot.getChildrenCount()];
                    String[] precios = new String[ (int) snapshot.getChildrenCount()];
                    String[] emails = new String[ (int) snapshot.getChildrenCount()];
                    String[] fechas = new String[ (int) snapshot.getChildrenCount()];

                    int index = 0;

                    for (DataSnapshot ds: snapshot.getChildren()){
                        nombres[index] = ds.child("nombreplato").getValue().toString();
                        cantidades[index] = ds.child("cantidad").getValue().toString();
                        precios[index] = ds.child("precioplato").getValue().toString();
                        emails[index]  = ds.child("emailusuario").getValue().toString();
                        fechas[index]  = ds.getKey();

                        index++;

                    }
                    GridAdapterReservaciones adapter = new GridAdapterReservaciones(Pedidos.this, nombres, precios, cantidades, emails, fechas);
                    _grid.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}