package facci.septimoa.comedoruniversitario;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetallePlato_admin extends AppCompatActivity {
    String key, imagen, nombre, precio, stock;
    TextView txtnombre, txtprecio, txtstock;
    ImageView grid_image, btneliminarplato;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_plato_admin);

        mDatabase = FirebaseDatabase.getInstance().getReference();

          txtnombre = findViewById(R.id.txtnombreplato);
          txtprecio = findViewById(R.id.txtprecioplato);
          txtstock = findViewById(R.id.txtstockplatos);
          grid_image = findViewById(R.id.grid_image);

        btneliminarplato = findViewById(R.id.btneliminarplato);


        Bundle bundle = getIntent().getExtras();
        key = bundle.getString("key");
        imagen = bundle.getString("imagen");
        nombre = bundle.getString("nombre");
        precio = bundle.getString("precio");
        stock = bundle.getString("stock");

        mostrarDetalle();

    btneliminarplato.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            eliminarPlato();
        }
    });
    }




    public void mostrarDetalle(){
        txtnombre.setText(nombre);
        txtprecio.setText("Precio: $"+precio);
        txtstock.setText("Stock: "+stock);
        Glide.with(DetallePlato_admin.this).load(imagen).fitCenter().centerCrop().into(grid_image);

    }

    public void eliminarPlato(){
        mDatabase = FirebaseDatabase.getInstance().getReference("Platos");
        mDatabase.child(key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    Toast.makeText(DetallePlato_admin.this, "Plato eliminado", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(DetallePlato_admin.this, Combos.class);
                    startActivity(i);
                    finish();

                }
            }
        });


    }
}