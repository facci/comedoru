package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {
    Button boton_enviar_registro;
    EditText txtnombre, txtapellido, txtdireccion, txtcorreo, txtpassword;
    String nombre, apellido,  direccion, correo, contrasena;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    ProgressDialog progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);


        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        boton_enviar_registro = findViewById(R.id.boton_enviar_registro);

        txtnombre = findViewById(R.id.txtnombre);
        txtapellido = findViewById(R.id.txtapellido);
        txtdireccion = findViewById(R.id.txtdireccion);
        txtcorreo = findViewById(R.id.txtcorreo);
        txtpassword = findViewById(R.id.txtpassword);


        progress = new ProgressDialog(this);

        /*ESTA ES LA ACCION QUE HACE EL BOTON DE REGISTRO Y EJECUTA EL METODO DE REGISTRAR USUARIO*/

        boton_enviar_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nombre = txtnombre.getText().toString();
                apellido = txtapellido.getText().toString();
                direccion = txtdireccion.getText().toString();
                correo = txtcorreo.getText().toString();
                contrasena = txtpassword.getText().toString();

                if (!nombre.isEmpty() &&!apellido.isEmpty() &&!direccion.isEmpty() &&!correo.isEmpty() &&!contrasena.isEmpty() ){
                    if (contrasena.length() < 6){
                        Toast.makeText(Registro.this, "La contraseña debe ser mayor o igual a 6 caracteres", Toast.LENGTH_LONG).show();

                    }else{
                        registrarUsuario();
                    }

                }else {
                    Toast.makeText(Registro.this, "Debe completar todos los campos", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /*ESTE METODO REGISTRA UN USUARIO EN FIREBASE
        * AUTENTICACION DE FIREBASE CON CORREO Y CONTRASEÑA
        * REALTIME DATABASE
    */
    private void registrarUsuario(){
        progress.setMessage("Registrando en linea");
        progress.show();
        mAuth.createUserWithEmailAndPassword(correo, contrasena).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Map<String, Object> map = new HashMap<>();
                    map.put("nombre", nombre);
                    map.put("apellido", apellido);
                    map.put("direccion", direccion);
                    map.put("correo", correo);
                    map.put("rol", "cliente");
                    map.put("contrasena", contrasena);
                    String id = mAuth.getCurrentUser().getUid();

                    mDatabase.child("Usuarios").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task2) {

                            if (task2.isSuccessful()) {
                                Intent i = new Intent(Registro.this, home.class);
                                startActivity(i);
                                finish();
                            } else {
                                Toast.makeText(Registro.this, "No se pudieron crear los datos", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                else {
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                        Toast.makeText(Registro.this, "Este usuario ya existe", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(Registro.this, "No se pudo registrar este usuario", Toast.LENGTH_LONG).show();
                    }
                }
                progress.dismiss();

            }

        });

    }
}