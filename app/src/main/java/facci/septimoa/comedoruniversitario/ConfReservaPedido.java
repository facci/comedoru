package facci.septimoa.comedoruniversitario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class   ConfReservaPedido extends AppCompatActivity {

    Button boton_confirmar_pedido;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conf_reserva_pedido);

        boton_confirmar_pedido = findViewById(R.id.boton_confirmar_pedido);
        boton_confirmar_pedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ConfReservaPedido.this, Codigo_Qr.class );
                startActivity(intent);
            }
        });
    }
}