package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

import facci.septimoa.comedoruniversitario.Adapter.GridAdapter;

public class home extends AppCompatActivity {

        Button perfil, menu, codigo_qr, cerrarSesion, btnregistroplato;
        FirebaseAuth mAuth;
        String user;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
        mAuth = FirebaseAuth.getInstance();

         user = mAuth.getCurrentUser().getEmail();

            menu = findViewById(R.id.menu);
        perfil =  findViewById(R.id.perfil);
            cerrarSesion = findViewById(R.id.cerrar_sesion);
        btnregistroplato = findViewById(R.id.btnregistroplato);



        perfil.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(home.this, PerfilUsuario.class );
                    startActivity(intent);
                    Log.d("USUARIOOOOO", user);
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(home.this, Combos.class );
                    startActivity(intent);
                }
            });



            // ESTE BOTON DE CERRAR LA SESION DEL USUARIO DE FIREBASE
            cerrarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FirebaseAuth.getInstance().signOut();
                    Intent i = new Intent(home.this, MainActivity.class);
                    startActivity(i);
                    finish();

                }
            });

            // ESTE BOTON DE CERRAR LA SESION DEL USUARIO DE FIREBASE
            cerrarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FirebaseAuth.getInstance().signOut();
                    Intent i = new Intent(home.this, MainActivity.class);
                    startActivity(i);
                    finish();

                }
            });
    }



}
