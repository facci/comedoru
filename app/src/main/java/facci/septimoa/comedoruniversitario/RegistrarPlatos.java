package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RegistrarPlatos extends AppCompatActivity {
    EditText txtnombreplato, txtprecio, txtstock;
    Button btnregistrarplato;
    String nombreplato, precioplato,  stockplatos, imagenparafirebase = "";
    ImageView imgplato;

    static final int GALLERY_INTENT = 1;
    StorageReference storageReference;
    String storage_path = "img/*";
    private Uri image_url;
    String photo = "photo";
    String idd;

    ProgressDialog progress;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_platos);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        storageReference = FirebaseStorage.getInstance().getReference();

        txtnombreplato = findViewById(R.id.txtnombreplato);
        txtprecio = findViewById(R.id.txtprecio);
        txtstock = findViewById(R.id.txtstock);
        btnregistrarplato = findViewById(R.id.btnregistrarplato);
        imgplato = findViewById(R.id.imgplato);

        progress = new ProgressDialog(this);


        /*ESTA ES LA ACCION QUE HACE EL BOTON DE REGISTRO Y EJECUTA EL METODO DE REGISTRAR PLATO*/

        btnregistrarplato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nombreplato = txtnombreplato.getText().toString();
                precioplato = txtprecio.getText().toString();
                stockplatos = txtstock.getText().toString();


                if (!nombreplato.isEmpty() && !precioplato.isEmpty() && !stockplatos.isEmpty() && !imagenparafirebase.isEmpty()){
                    registrarPlato();


                }else {
                    Toast.makeText(RegistrarPlatos.this, "Debe completar todos los campos", Toast.LENGTH_LONG).show();
                }
            }
        });

        // seleccionar imagen de galeria
        imgplato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent galeria = new Intent(Intent.ACTION_PICK);
            galeria.setType("image/*");
            startActivityForResult(galeria, GALLERY_INTENT);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_INTENT && resultCode == RESULT_OK) {
            progress.setMessage("Subiendo imagen");
            progress.show();
            image_url = data.getData();
            StorageReference filePath = storageReference.child("img");
            filePath.putFile(image_url).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> uriTask = taskSnapshot.getStorage().getDownloadUrl();
                    uriTask.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            imagenparafirebase = uri.toString();
                            Glide.with(RegistrarPlatos.this).load(uri).fitCenter().centerCrop().into(imgplato);
                            progress.dismiss();
                            Toast.makeText(RegistrarPlatos.this, "Imagen cargada", Toast.LENGTH_LONG).show();
                        }
                    });

                }

            }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });
    }

    }



    /*ESTE METODO REGISTRA UN USUARIO EN FIREBASE
     * AUTENTICACION DE FIREBASE CON CORREO Y CONTRASEÑA
     * REALTIME DATABASE
     */
    private void registrarPlato(){
        progress.setMessage("Registrando en linea");
        progress.show();
        SimpleDateFormat fechaEspanol = new SimpleDateFormat("dd MMM yyyy - HH:mm:ss", Locale.forLanguageTag("es_ES"));

        Map<String, Object> map = new HashMap<>();
                    map.put("nombreplato", nombreplato);
                    map.put("precioplato", precioplato);
                    map.put("stockplatos", stockplatos);
        map.put("imagenparafirebase", imagenparafirebase);



                    mDatabase.child("Platos").child(fechaEspanol.format(new Date())).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {

                            if (task.isSuccessful()) {
                                Intent i = new Intent(RegistrarPlatos.this, Combos.class);
                                startActivity(i);
                                finish();
                                Toast.makeText(RegistrarPlatos.this, "Registro creado", Toast.LENGTH_LONG).show();

                            } else {
                                Toast.makeText(RegistrarPlatos.this, "No se pudieron crear los datos", Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                progress.dismiss();



    }
}