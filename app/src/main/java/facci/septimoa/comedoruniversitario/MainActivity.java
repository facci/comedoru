package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    Button  btnRegistar, btnIngresar;
    FirebaseAuth mAuth;
    DatabaseReference mDatabase;

    private GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;
    String key;

    /*
    PERSISTENCIA DE DATOS, VERIFICA SI EXISTE UN USUARIO LOGUEADO DESDE FIREBASE Y MANTIENE LOS
    DATOS DE ESE USUARIO
    */
    protected void onStart(){
        super.onStart();

        FirebaseUser user = mAuth.getCurrentUser();
        if(user==null){
            Intent i = new Intent(MainActivity.this, InicioSeccion.class);
            startActivity(i);
            finish();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id2))
                .requestEmail()
                .build();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        btnRegistar = findViewById(R.id.btnRegistar);
        btnIngresar =  findViewById(R.id.btnIngresar);

        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null) {
            if (user.getEmail().equals("admin@admin.com")) {
                Intent i = new Intent(MainActivity.this, home_admin.class);
                startActivity(i);
                finish();
            }else {
                Intent i = new Intent(MainActivity.this, home.class);
                startActivity(i);
                finish();
            }

        }



        btnRegistar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, Registro.class );
                startActivity(intent);
            }
        });

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, InicioSeccion.class );
                startActivity(intent);
            }
        });
    }

}