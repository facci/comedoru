package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import facci.septimoa.comedoruniversitario.Adapter.GridAdapter;

public class Combos extends AppCompatActivity {

    DatabaseReference mDatabase;
    GridView _grid;
    FirebaseAuth mAuth;
    String email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combos);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        email = mAuth.getCurrentUser().getEmail();
        _grid = findViewById(R.id.gridView);

        obtenerMenu();

    }

    public void obtenerMenu(){
        mDatabase.child("Platos").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                if (snapshot.exists()){
                    String[] nombres = new String[ (int) snapshot.getChildrenCount()];
                    String[] imagenes = new String[ (int) snapshot.getChildrenCount()];
                    String[] precios = new String[ (int) snapshot.getChildrenCount()];
                    String[] stocks = new String[ (int) snapshot.getChildrenCount()];;
                    String[] keys = new String[ (int) snapshot.getChildrenCount()];;

                    int index = 0;

                    for (DataSnapshot ds: snapshot.getChildren()){
                        nombres[index] = ds.child("nombreplato").getValue().toString();
                        imagenes[index] = ds.child("imagenparafirebase").getValue().toString();
                        precios[index]  = ds.child("precioplato").getValue().toString();
                        stocks[index]  = ds.child("stockplatos").getValue().toString();
                        keys[index]  = ds.getKey();
                        index++;

                    }
                    GridAdapter adapter = new GridAdapter(Combos.this, nombres, imagenes, precios, stocks, keys, email);
                    _grid.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}

