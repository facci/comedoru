package facci.septimoa.comedoruniversitario;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Codigo_Qr extends AppCompatActivity {

    Button boton_codigo_qr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codigo_qr);

        boton_codigo_qr = findViewById(R.id.boton_codigo_qr);

        boton_codigo_qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Codigo_Qr.this, home.class );
                startActivity(intent);
            }
        });
    }
}