package facci.septimoa.comedoruniversitario;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class PerfilUsuario extends AppCompatActivity {
TextView nombre, apellido, direccion, email;

    FirebaseAuth mAuth;
    DatabaseReference mDatabase;
    String idUsuario;

    private GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id2))
                .requestEmail()
                .build();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

       idUsuario = mAuth.getCurrentUser().getUid();


        nombre = findViewById(R.id.nombreusuario);
        //apellido = findViewById(R.id.apellidousuario);
        direccion = findViewById(R.id.direccionusuario);
        email = findViewById(R.id.emailusuario);

        obtenerUsuario(idUsuario);



    }


    void obtenerUsuario(String id) {
        mDatabase.child("Usuarios").child(id).get().addOnCompleteListener(new OnCompleteListener<DataSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DataSnapshot> task) {
                if (!task.isSuccessful()) {
                }
                else {
                    String n=  task.getResult().child("nombre").getValue().toString();
                    String a=  task.getResult().child("apellido").getValue().toString();
                    //nombres.setText(task.getResult().child("nombre").getValue().toString());
                    nombre.setText(n +"  "+ a);
                   // apellido.setText(task.getResult().child("apellido").getValue().toString());
                    direccion.setText(task.getResult().child("direccion").getValue().toString());
                    email.setText(task.getResult().child("correo").getValue().toString());

                }
            }
        });

    }
}